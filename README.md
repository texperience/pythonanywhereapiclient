# pythonanywhereapiclient

[![Pipeline][pipeline-badge]][pipeline-link]
[![Coverage][coverage-badge]][coverage-link]
[![PyPI][pypi-badge]][pypi-link]

[pipeline-badge]: https://gitlab.com/texperience/pythonanywhereapiclient/badges/master/pipeline.svg
[pipeline-link]: https://gitlab.com/texperience/pythonanywhereapiclient/pipelines
[coverage-badge]: https://gitlab.com/texperience/pythonanywhereapiclient/badges/master/coverage.svg
[coverage-link]: https://gitlab.com/texperience/pythonanywhereapiclient/-/jobs
[pypi-badge]: https://img.shields.io/pypi/v/pythonanywhereapiclient.svg
[pypi-link]: https://pypi.python.org/pypi/pythonanywhereapiclient

Thin wrapper for the [PythonAnywhere API][pa-api-docs].

[pa-api-docs]: https://help.pythonanywhere.com/pages/API

## Features

* Basic API client responsible for authentication and sending requests
* List and configure webapps
* List and upload files
* List and configure schedules
* List and work with consoles

## Technical requirements

Below is the list of currently supported Python releases:

| # | Python |
|---|--------|
| 1 | 3.7    |
| 2 | 3.8    |
| 3 | 3.9    |

## Code and contribution

The code is open source and released under the [MIT License (MIT)][mit-license]. It is available on [Gitlab][gitlab] and follows the guidelines about [Semantic Versioning][semver] for transparency within the release cycle and backward compatibility whenever possible.

All contributions are welcome, whether bug reports, reviews, documentation or feature requests.

If you're a developer and have time and ideas for code contributions, fork the repo and prepare a merge request:

```bash
# Prepare your environment the first time
mkvirtualenv --python=python3.8 pythonanywhereapiclient-py38
pip install -e .[testing]

# Running the tests while development
pytest

# Individual Python release tests and code quality checks
tox -e py38
tox -e coverage
tox -e isort
tox -e brunette
tox -e flake8

# Ensure code quality running the entire test suite,
# this requires all supported Python releases to be installed
tox
```

[mit-license]: https://en.wikipedia.org/wiki/MIT_License
[gitlab]: https://gitlab.com/texperience/pythonanywhereapiclient
[semver]: http://semver.org/

## Getting Started

This document provides all the basic information you need to start using the client. It covers base concepts, shows examples, and documents links for further reading.

### Setup

There are a few setup steps you need to complete before you can proceed:

1.  [Sign up][pa-signup] for a PythonAnywhere account, if you don't already have one.
1.  [Activate][pa-api-docs] your personal API token.

[pa-signup]: https://www.pythonanywhere.com/login

### Installation

Install `pythonanywhereapiclient` using pip:

```bash
pip install pythonanywhereapiclient
```

### Configuration

The client simply needs three environment variables to work properly:

* `PYTHONANYWHERE_API_CLIENT_HOST`
* `PYTHONANYWHERE_API_CLIENT_TOKEN`
* `PYTHONANYWHERE_API_CLIENT_USER`

For example to set these environment variables in a bash:

```bash
export PYTHONANYWHERE_API_CLIENT_HOST="www.pythonanywhere.com"
export PYTHONANYWHERE_API_CLIENT_TOKEN="yoursecrettoken"
export PYTHONANYWHERE_API_CLIENT_HOST="yourusername"
```

Valid values for `PYTHONANYWHERE_API_CLIENT_HOST` are `www.pythonanywhere.com` and `eu.pythonanywhere.com`.

### Usage

The following examples showcase some typical usage scenarios.

```python
from pythonanywhereapiclient import console, file, schedule, webapp

# Create a webapp
webapp.create('example.com', 'python36')

# List all webapps
webapp.list()

# Modify an existing webapp
webapp.modify(
    'example.com',
    python_version='3.6',
    working_directory='/home/user/work',
    source_directory='/home/user/source',
    virtualenv_path='/home/user/virtualenv',
    force_https=True
)

# Create a static mapping
webapp.create_static('example.com', '/static/', '/home/user/data/static')

# Reload a webapp (e.g. after source or configuration changes)
webapp.reload('example.com')

# Disable a webapp
webapp.disable('example.com')

# Enable a webapp
webapp.enable('example.com')

# Delete a webapp
webapp.delete('example.com')

# List all files at path
file.list('/home/username/data')

# Upload a file
file.upload('/home/username/data/foo.txt', 'this is a text file')

# Create a schedule
schedule.create(command='clear', enabled=True, interval='daily', hour=12, minute=59)

# List all schedules
schedule.list()

# Delete a schedule
schedule.delete(id=1)

# Create a console
console.create(executable='bash', working_directory='/home')

# List all consoles
console.list()

# Send input to a console
console.send_input(id=1, input='whoami\n')

# Get latest output from a console
console.get_latest_output(id=1)

# Kill a console
console.kill(id=1)
```
