from pytest import raises
from requests import get

from pythonanywhereapiclient.error import BaseError, QuotaError, ResponseError


def test_baseerror():
    with raises(BaseError):
        raise BaseError()


def test_quotaerror_enforces_message():
    with raises(TypeError) as exc:
        raise QuotaError()

    assert 'required positional argument' in str(exc.value)
    assert 'message' in str(exc.value)


def test_responseerror_requires_response_as_argument():
    with raises(TypeError) as exc:
        raise ResponseError()

    assert 'required positional argument' in str(exc.value)
    assert 'response' in str(exc.value)


def test_responseerror_sets_message(responses):
    status_code = 500
    text = 'testtext'
    responses.add('GET', 'http://foo', body=text, status=status_code)

    with raises(ResponseError) as exc:
        raise ResponseError(get('http://foo'))

    assert 'Unexpected HTTP response' in str(exc.value)
    assert str(status_code) in str(exc.value)
    assert text in str(exc.value)
