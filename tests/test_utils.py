import os

from pythonanywhereapiclient.utils import (
    construct_request_payload,
    getenv,
    Python,
)


def test_construct_request_payload_allows_falsy_values():
    payload = {
        'command': '',
        'enabled': False,
        'hour': 0,
    }

    assert payload == construct_request_payload(payload)


def test_construct_request_payload_allows_truthy_values():
    payload = {
        'command': 'sleep 10',
        'enabled': True,
        'hour': 10,
    }

    assert payload == construct_request_payload(payload)


def test_construct_request_payload_excludes_given_keys():
    payload = {
        'command': 'sleep 10',
        'enabled': True,
        'hour': 10,
        'excl_boolean': True,
        'excl_int': 10,
        'excl_none': None,
        'excl_string': 'sleep 20',
    }

    expected_payload = payload.copy()
    del expected_payload['excl_boolean']
    del expected_payload['excl_int']
    del expected_payload['excl_none']
    del expected_payload['excl_string']

    assert expected_payload == construct_request_payload(
        payload,
        exclude=['excl_boolean', 'excl_int', 'excl_none', 'excl_string'],
    )


def test_construct_request_payload_removes_none_values():
    payload = {
        'command': None,
        'enabled': True,
        'hour': 0,
    }
    expected_payload = payload.copy()
    del expected_payload['command']

    assert expected_payload == construct_request_payload(payload)


def test_getenv_enforces_namespace():
    os.environ['PYTHONANYWHERE_API_CLIENT_FOO'] = 'bar'

    assert getenv('FOO') == 'bar'


def test_python_version_constants():
    python = Python(version='3.8')

    assert python.executable == 'python3.8'
    assert python.longcode == 'python38'
    assert python.shortcode == 'py38'
