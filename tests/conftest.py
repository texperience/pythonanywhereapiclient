import pytest
from responses import RequestsMock


base_data = {
    # Account
    'host': 'testhost',
    'token': 'testtoken',
    'user': 'testuser',
    # Response bodies
    'body_ok': {'status': 'OK'},
    'body_unknown_error': {'error_message': 'Unknown error'},
    # Webapp
    'domain_name': 'www.testdomain.de',
}


@pytest.fixture
def data():
    return base_data


@pytest.fixture(autouse=True)
def env(monkeypatch):
    namespace = 'PYTHONANYWHERE_API_CLIENT_'
    monkeypatch.setenv(f'{namespace}HOST', base_data['host'])
    monkeypatch.setenv(f'{namespace}TOKEN', base_data['token'])
    monkeypatch.setenv(f'{namespace}USER', base_data['user'])


@pytest.fixture
def responses():
    with RequestsMock() as responses:
        yield responses
